import argparse
from scrape import Scraper

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scrape some data.')
    parser.add_argument("--url", required=False, help="start page")
    parser.add_argument("--limit_book", required=False, help="limit of book")
    parser.add_argument("--limit_author", required=False, help="limit of author")


    args = parser.parse_args()

    start_page = "https://www.goodreads.com/book/show/3735293-clean-code?from_search=true&qid=HhMDV0vMa5&rank=1"
    book = 2000
    author = 2000

    if args.url:
        start_page = args.url
    if args.limit_book:
        book = args.limit_book
    if args.limit_author:
        author = args.limit_author

    scraper = Scraper(start_page, int(book), int(author))
    scraper.start()
