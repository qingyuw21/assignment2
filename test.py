from scrape import Scraper
import unittest
import os

class TestScraper(unittest.TestCase):

    def test_init(self):
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 50, 10)
        self.assertEqual(scraper.num_authors, 0)
        self.assertEqual(scraper.num_authors, 0)
        self.assertEqual(scraper.limit_books, 50)
        self.assertEqual(scraper.limit_authors, 10)
    
    def test_scrape_one_book(self):
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 1, 0)
        scraper.scrape_book(start_page)
        self.assertEqual(scraper.data["Books"][0]["title"], "Clean Code: A Handbook of Agile Software Craftsmanship") 
        self.assertEqual(scraper.data["Books"][0]["book_id"], "3735293")
    
    def test_scrape_one_author(self):
        start_page = "https://www.goodreads.com/author/show/45372.Robert_C_Martin"
        scraper = Scraper(start_page, 1, 1)
        scraper.scrape_author(start_page)
        self.assertEqual(scraper.data["Authors"][0]["name"], "Robert C. Martin")
        self.assertEqual(scraper.data["Authors"][0]["author_id"], "45372")

    def test_scrape_multiple_book(self):
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 3, 1)
        scraper.scrape_book(start_page)
        self.assertEqual(scraper.data["Books"][0]["title"], "Clean Code: A Handbook of Agile Software Craftsmanship") 
        self.assertEqual(scraper.data["Books"][0]["book_id"], "3735293")
        self.assertEqual(scraper.data["Books"][1]["title"], "The Clean Coder: A Code of Conduct for Professional Programmers") 
        self.assertEqual(scraper.data["Books"][1]["book_id"], "10284614")
        self.assertEqual(scraper.data["Books"][2]["title"], "Clean Architecture") 
        self.assertEqual(scraper.data["Books"][2]["book_id"], "18043011")
    
    def test_scrape_multiple_author(self):
        start_page = "https://www.goodreads.com/author/show/45372.Robert_C_Martin"
        scraper = Scraper(start_page, 0, 3)
        scraper.scrape_author(start_page)
        self.assertEqual(scraper.data["Authors"][0]["name"], "Robert C. Martin")
        self.assertEqual(scraper.data["Authors"][0]["author_id"], "45372")
        self.assertEqual(scraper.data["Authors"][1]["name"], "Andy Hunt")
        self.assertEqual(scraper.data["Authors"][1]["author_id"], "2815")
        self.assertEqual(scraper.data["Authors"][2]["name"], "Chad Fowler")
        self.assertEqual(scraper.data["Authors"][2]["author_id"], "302")

    def test_scrape_author_books(self):
        start_page = "https://www.goodreads.com/author/list/45372.Robert_C_Martin"
        scraper = Scraper("", 1, 0)

        ret = [
            'https://www.goodreads.com/book/show/3735293-clean-code', 
            'https://www.goodreads.com/book/show/10284614-the-clean-coder', 
            'https://www.goodreads.com/book/show/18043011-clean-architecture', 
            'https://www.goodreads.com/book/show/25099258-the-art-of-unit-testing', 
            'https://www.goodreads.com/book/show/84985.Agile_Software_Development_Principles_Patterns_and_Practices', 
            'https://www.goodreads.com/book/show/84983.Agile_Principles_Patterns_and_Practices_in_C_', 
            'https://www.goodreads.com/book/show/45280021-clean-agile', 
            'https://www.goodreads.com/book/show/13136186-the-robert-c-martin-clean-code-collection', 
            'https://www.goodreads.com/book/show/25109583-manifesto-for-agile-software-development', 
            'https://www.goodreads.com/book/show/25576965-java-application-architecture', 
            'https://www.goodreads.com/book/show/873375.UML_for_Java_Programmers', 
            'https://www.goodreads.com/book/show/177638.The_Cambridge_Encyclopedia_of_Human_Evolution', 
            'https://www.goodreads.com/book/show/79774.Pattern_Languages_of_Program_Design_3', 
            'https://www.goodreads.com/book/show/1640115.Extreme_Programming_In_Practice', 
            'https://www.goodreads.com/book/show/25936819-design-principles-and-design-patterns', 
            'https://www.goodreads.com/book/show/2075684.Designing_C_Applications_Using_the_Booch_Notation', 
            'https://www.goodreads.com/book/show/2503989.RibbonX', 
            'https://www.goodreads.com/book/show/26091056-lsp', 
            'https://www.goodreads.com/book/show/131570.Macromedia_Director_MX_2004_Bible_With_CDROM_', 
            'https://www.goodreads.com/book/show/4828094-director-8-and-lingo-bible-with-cdrom', 
            'https://www.goodreads.com/book/show/26091039-ocp', 
            'https://www.goodreads.com/book/show/26091146-dip', 
            'https://www.goodreads.com/book/show/26091115-isp', 
            'https://www.goodreads.com/book/show/26091010-srp', 
            'https://www.goodreads.com/book/show/16256760-excel-programming-with-vba-starter', 
            'https://www.goodreads.com/book/show/54594740-clean-agile', 
            'https://www.goodreads.com/book/show/34529639-sebevrahuv-pr-vodce-prahou', 
            'https://www.goodreads.com/book/show/15561963-practical-speech-for-modern-business', 
            'https://www.goodreads.com/book/show/45878091-evil-given-for-evil-done', 
            'https://www.goodreads.com/book/show/55320314-czysty-agile-powr-t-do-podstaw'
            ]

        out = scraper.scrape_author_books(start_page)
        self.assertCountEqual(out, ret)

    def test_scrape_related_authors(self):
        start_page = "https://www.goodreads.com/author/similar/45372.Robert_C_Martin"
        scraper = Scraper("", 1, 1)
        ret = [
            'https://www.goodreads.com/author/show/2815.Andy_Hunt', 
            'https://www.goodreads.com/author/show/3307.Steve_McConnell', 
            'https://www.goodreads.com/author/show/25201.Michael_C_Feathers', 
            'https://www.goodreads.com/author/show/25211.Kent_Beck', 
            'https://www.goodreads.com/author/show/25215.Martin_Fowler', 
            'https://www.goodreads.com/author/show/32731.Eric_Freeman', 
            'https://www.goodreads.com/author/show/48622.Erich_Gamma', 
            'https://www.goodreads.com/author/show/48660.Joshua_Kerievsky', 
            'https://www.goodreads.com/author/show/104368.Eric_Evans', 
            'https://www.goodreads.com/author/show/7127583.Sandro_Mancuso'
            ]
        
        out = scraper.scrape_related_authors(start_page)
        self.assertCountEqual(out, ret)

    def test_write_to_json(self):
        if os.path.exists("data.json"):
            os.remove("data.json")
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 1, 0)
        scraper.scrape_book(start_page)
        scraper.write_to_json()
        self.assertTrue(os.stat("data.json").st_size != 0)

    
    def test_read_from_json(self):
        if os.path.exists("data.json"):
            os.remove("data.json")
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 1, 1)
        scraper.scrape_book(start_page)
        scraper.write_to_json()

        scraper_two = Scraper("", 0, 0)
        scraper_two.read_from_json()
        self.assertEqual(scraper.data["Books"][0]["title"], "Clean Code: A Handbook of Agile Software Craftsmanship") 
        self.assertEqual(scraper.data["Books"][0]["book_id"], "3735293")
        self.assertEqual(scraper.data["Authors"][0]["name"], "Robert C. Martin")
        self.assertEqual(scraper.data["Authors"][0]["author_id"], "45372")

    def test_start(self):
        start_page = "https://www.goodreads.com/book/show/3735293-clean-code"
        scraper = Scraper(start_page, 1, 1)
        scraper.start()
        self.assertEqual(scraper.data["Books"][0]["title"], "Clean Code: A Handbook of Agile Software Craftsmanship") 
        self.assertEqual(scraper.data["Books"][0]["book_id"], "3735293")
        self.assertEqual(scraper.data["Authors"][0]["name"], "Robert C. Martin")
        self.assertEqual(scraper.data["Authors"][0]["author_id"], "45372")

if __name__ == '__main__':
    unittest.main()