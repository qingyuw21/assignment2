import requests
import json
import pymongo
import os

from dotenv import load_dotenv
from bs4 import BeautifulSoup
from copy import deepcopy

load_dotenv()

CONNECTION = os.getenv("CONNECTION")

class Scraper:

    def __init__(self, page, limit_books, limit_authors):
        # init all the varibles
        self.page = page
        self.num_books = 0
        self.num_authors = 0
        self.books = []
        self.authors = []
        self.data = {"Books": [], "Authors": []}

        self.limit_books = limit_books
        self.limit_authors = limit_authors

        # connect to db
        self.client = pymongo.MongoClient(CONNECTION)
        self.db = self.client.myDB

    # start scraping
    def start(self):
        self.scrape_book(self.page)
        self.write_to_json()

    # scrape book from this url
    def scrape_book(self, book):
        # don't scrape if already scrape or approach limit
        if book in self.books or self.num_books >= self.limit_books:
            return

        # get html file from this url
        page = requests.get(book)
        soup = BeautifulSoup(page.content, "html.parser")

        metadata = {}

        # store everything we need from this html
        metadata["book_url"] = book
        metadata["title"] = soup.find(id="bookTitle").text.strip()
        metadata["book_id"] = soup.find("input", {"id": "book_id"}).get("value")
        metadata["isbn"] = soup.find("meta", {"property": "books:isbn"})["content"]
        metadata["author_url"] = soup.find("meta", property="books:author")["content"]
        metadata["author"] = soup.find("span", itemprop="name").text
        metadata["rating"] = soup.find("span", itemprop="ratingValue").text.strip()
        metadata["rating_count"] = soup.find("meta", itemprop="ratingCount")["content"]
        metadata["review_count"] = soup.find("meta", itemprop="reviewCount")["content"]
        metadata["image_url"] = soup.find("div", {"class": "bookCoverPrimary"}).img["src"]
        similar_books = []
        temp = soup.find_all("li", {"class": "cover"})
        for i in temp:
            similar_books.append(i.a['href'])
        metadata["similar_books"] = similar_books

        # increment book count and add url to the list
        self.books.append(book)
        self.num_books += 1
        self.data["Books"].append(metadata)
        print("Scraped {}. Total {} books scraped".format(metadata["title"], self.num_books))

        # write data to database
        self.db.books.insert_one(deepcopy(metadata))

        # scrape author page
        try:
            self.scrape_author(metadata["author_url"])
        except:
            return

    # scrape author from this url
    def scrape_author(self, author_url):
         # don't scrape if already scrape or approach limit
        if author_url in self.authors or self.num_authors >= self.limit_authors:
            return

        # get html file from this url
        page = requests.get(author_url)
        soup = BeautifulSoup(page.content, "html.parser")

        metadata = {}

        # store everything we need from this html
        metadata["name"] = soup.find("span", itemprop="name").text
        metadata["author_url"] = author_url
        metadata["author_id"] = author_url.split("show/")[1].split(".")[0]
        metadata["rating"] = soup.find("span", itemprop="ratingValue").text
        metadata["rating_count"] = soup.find("span", itemprop="ratingCount").text.strip()
        metadata["review_count"] = soup.find("span", itemprop="reviewCount").text.strip()
        metadata["image_url"] = soup.find('div', {"class": "leftContainer authorLeftContainer"}).img['src']

        related_authors_url = ""
        author_books_url = ""
        links = soup.find_all("a", href=True)
        for i in links:
            if "list" in i["href"]:
                author_books_url = "https://www.goodreads.com" + i["href"]
            if "similar" in i["href"]:
                related_authors_url = "https://www.goodreads.com" + i["href"]
                break

        # scrape related author
        metadata["related_authors"] = self.scrape_related_authors(related_authors_url)
        # scrape authors book
        metadata["authors_books"] = self.scrape_author_books(author_books_url)

        # increment author count and add url to the list
        self.authors.append(author_url)
        self.num_authors += 1
        self.data["Authors"].append(metadata)
        print("Scraped {}. Total {} authors scraped".format(metadata["name"], self.num_authors))

        # write data to database
        self.db.authors.insert_one(deepcopy(metadata))

        # scrape this author's books
        for book in metadata["authors_books"]:
            try:
                self.scrape_book(book)
            except:
                continue

        # scrape similiar authors
        for author in metadata["related_authors"]:
            try:
                self.scrape_author(author)
            except:
                continue

    # scrape related authors
    def scrape_related_authors(self, related_url):
        page = requests.get(related_url)
        soup = BeautifulSoup(page.content, "html.parser")

        related_authors = []
        temp = soup.find_all("a", {"class": "gr-h3 gr-h3--serif gr-h3--noMargin"})
        for i in temp:
            related_authors.append(i["href"])

        return related_authors[1:]

    # scrape author's books
    def scrape_author_books(self, books_url):
        page = requests.get(books_url)
        soup = BeautifulSoup(page.content, "html.parser")

        author_books = []
        temp = soup.find_all("a", {"class": "bookTitle"})
        for i in temp:
            author_books.append("https://www.goodreads.com" + i["href"])

        return author_books

    # write all the data to json file
    def write_to_json(self):
        with open('data.json', 'a') as f:
            json.dump(self.data, f)

    # read data from json file
    def read_from_json(self):
        try:
            with open('data.json', 'r') as f:
                self.data = json.load(f)
        except IOError:
            return

        for book in self.data["Books"]:
            self.books.append(book["book_url"])
            self.num_books += 1

        for author in self.data["Authors"]:
            self.authors.append(author["author_url"])
            self.num_authors += 1
